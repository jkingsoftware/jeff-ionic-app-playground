import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RoutingConstants as rc} from '../app/shared/constant/routing-constant';
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: rc.LOGIN_ROUTE
  },
  {
    path: rc.LOGIN_ROUTE,
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
  },
  {
    path: rc.MENU_ROUTE,
    loadChildren: () => import('./modules/menu/menu.module').then(m => m.MenuModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
